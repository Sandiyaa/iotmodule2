**EMBEDDED SYSTEMS**

- An embedded system is a combination of computer hardware and software designed for a specific function or functions within a larger system. 
- The systems can be programmable or with fixed functionality.
- An embedded system is a microprocessor-based computer hardware system with software that is designed to perform a dedicated function, either as an independent system or as a part of a large system.
- At the core is an integrated circuit designed to carry out computation for real-time operations.The systems can be programmable or with fixed functionality.
- The complexity of an embedded system varies significantly depending on the task for which it is designed.
- Typically, IoT devices are embedded systems. In IoT, the devices which have been embedded with electronics, internet connectivity and other forms of hardware like sensors can communicate and interact with other devices over the internet.

![](https://www.livewireindia.com/blog/wp-content/uploads/2019/04/EMBEDDED-SYSTEM-COURSE-1024x537.jpg)

**EMBEDDED SYSTEMS FOUND IN MOST ELECTRONICS DEVICES**

- Embedded systems are found in many devices in common use today. 
- The capabilities provided by embedded systems enable electronic equipment to have far greater capabilities than would be possible if only hardware techniques were used.
- As a result, embedded systems are found in all manner of electronic equipment and gadgets.

 ![](https://rh6stzxdcl1wf9gj1fkj14uc-wpengine.netdna-ssl.com/wp-content/uploads/2017/03/Fig1-circuit-board-973311__340.jpg)

Below are the basic electronic devices used in embedded systems.

**1. SENSORS AND ACTUATORS**

- The intelligence and value from an IoT system is based on what can be learned from the data. Sensors are the source of IoT data.
- A better term for a sensor is a **transducer**. A transducer is any physical device that converts one form of energy into another.
- So, in the case of a sensor, the transducer converts some physical phenomenon into an electrical impulse that can then be interpreted to determine a reading.

- Another type of transducer that encounters in many IoT systems is an actuator.
- In simple terms, an actuator operates in the reverse direction of a sensor.
- It takes an electrical input and turns it into physical action.

![](https://image.slidesharecdn.com/01-internet-of-things-introduction-to-internet-of-things-171008183326/95/01-internetofthingsintroductiontointernetofthings-21-638.jpg?cb=1507494158)

In general, there are many sensors in an IoT embedded systems. They are -
- Temperature Sensor
- Humidity Sensor
- Motion Sensor
- Gas sensor
- Smoke Sensor
- Pressure Sensor
- Image Sensor and many more.

The below picture depicts the types of actuators in embedded systems

![](https://image1.slideserve.com/2043207/types-of-actuators-l.jpg)

**WORKING OF SENSORS AND ACTUATORS**

![](https://bridgera.com//wp-content/uploads/2017/06/sensor_actuator_graphicssec1_pg16.jpg)

**2. ANALOG AND DIGITAL**

- Analog and digital signals are different types which are mainly used to carry the data from one apparatus to another.
- Analog signals are continuous wave signals that change with time period whereas digital is a discrete signal is a nature.
- The main difference between analog and digital signals is, analog signals are represented with the sine waves whereas digital signals are represented with square waves.
- Let us discuss some dissimilarity of analog & digital signals.

![](https://www.rfwireless-world.com/images/Analog-Signal-vs-Digital-Signal.jpg)

The diference between the analog and digital signals.

![](https://media.cheggcdn.com/study/870/8701e007-8704-4de7-8e43-88799d198442/10298-5.E-6LOC-i1.png)


**3. MICROPROCESSORS AND MICROCONTROLLERS**

- An integrated circuit contained on a single silicon chip, a **microprocessor** contains the arithmetic logic unit, control unit, internal memory registers, and other vital circuitry of a computer's central processing unit (CPU).
 - Microprocessor commonly is used interchangeably with CPU and processor.

![](https://eeeproject.com/wp-content/uploads/2017/10/Microprocessor-vs-Microcontroller.jpg)

- A **microcontroller** is a compact integrated circuit designed to govern a specific operation in an embedded system.
- It is an integrated circuit (IC) device used for controlling other portions of an electronic system, usually via a microprocessor unit (MPU), memory, and some peripherals.

**4. RASPBERRY Pi**

- Raspberry Pi is the name of a series of single-board computers made by the Raspberry Pi Foundation, a UK charity that aims to educate people in computing and create easier access to computing education. It is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse.

![](https://www.raspberrypi.org/homepage-9df4b/static/hero-shot-33d83b8c5fa0933373dabcc9462b32a3.png)

In general, Raspberry Pi is a

- Mini Computer
- Limited but large power for its size
- No storage
- It is a SOC (System On Chip)
- We can connect shields (Shields - addon functionalities)
- Can connect multiple Pi’s together
- Microprocessor
- Can load a linux OS on it
- Pi uses ARM
- Connect to sensors or actuators

**INTERFACES** 

![](https://www.rs-online.com/designspark/rel-assets/dsauto/temp/uploaded/githubpin.JPG)

**5. PARALLEL AND SERIAL COMMUNICATION**

![](https://www.differencebetween.com/wp-content/uploads/2018/05/Difference-Between-Serial-and-Parallel-Communication-Comparison-Summary.jpg)

- PARALLEL INTERFACES - GPIO
- SERIAL INTERFACES - UART, SPI, I2C



