**IIOT PROTOCOLS**

- **IIOT**(*INDUSTRIAL INTERNET OF THINGS*)

![](https://optiware.com/wp-content/uploads/2019/06/IIoT-small-image.jpg)

- IoT communication protocols are modes of communication that protect and ensure optimum security to the data being exchanged between connected devices. 

**1. 4-20mA CURRENT LOOP**

- The 4-20 mA current loop has been the standard for signal transmission and electronic control in control systems.
- In a current loop, the current signal is drawn from a dc power supply, flows through the transmitter, into the controller and then back to the power supply in a series circuit.
- The below picture shows components of 4-20mA current loop.

   ![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQELnWB_SdBp5fLyzCnOg7taBeFR7Uoi8DV9g&usqp=CAU)

- **Advantages**
1. It can be run over long distances with minimal signal losses compared to voltage type signals
2. A varying current loop load impedance or supply voltage will not significantly affect the signal as long as it does not exceed recommended component limits
3. Rugged signal with low electromagnetic susceptibility
4. Saves on cable wire because it only needs 2 wires to function
5. Live zero reading verifies sensor is electrically functional

- **Disadvantages**
1. High power consumption compared to other analogue signal types
2. Elevated output at zero reading
3. Supply not isolated from output
4. Increasing circuit load resistance, will reduce the supply voltage available to power the transmitter that is generating the 4-20mA signal.

**2. MODBUS PROTOCOL**

- MODBUS Protocol is a messaging structure, widely used to establish master-slave communication between intelligent devices.
- Since Modbus protocol is just a messaging structure, it is independent of the underlying physical layer. It is traditionally implemented using RS232, RS422, or RS485.
- In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.

![](https://i0.wp.com/habrastorage.org/webt/bb/c7/yu/bbc7yu_iqs8ucr9ofnja0lv48fm.png?w=500&ssl=1)

**HOW DOES MODBUS PROTOCOL WORKS?**

- Communication between a master and a slave occurs in a frame that indicates a function code.
- The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.

![](https://i.ytimg.com/vi/txi2p5_OjKU/maxresdefault.jpg)

- The slave then responds, based on the function code received.
- The protocol is commonly used in IoT as a local interface to manage devices.
- Modbus protocol can be used over 2 interfaces. They are -

1. RS485 - called as Modbus RTU - RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
2. Ethernet - called as Modbus TCP/IP - Ethernet protocol is a typical LAN technology.

**3. OPC UA PROTOCOL**

- OPC Unified Architecture (OPC UA) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation.
- It is one of the most important communication protocols for Industry 4.0 and the IoT.

![](https://www.novotek.com/images/solutionpages/Kepware_solutionpages/2015_OPC_client_server.png)

- With OPC, access to machines, devices and other systems in the industrial environment is standardized and enables similar and manufacturer-independent data exchange.

- The picture below explains the OPU CA client and server model.

![](https://www.novotek.com/images/solutionpages/Kepware_solutionpages/2015_OPCmatrix.png)

**4. CLOUD PROTOCOLS (MQTT AND HTTP)**

- Cloud IoT Core supports two protocols for device connection and communication: MQTT and HTTP. Devices communicate with Cloud IoT Core across a "bridge" — either the **MQTT** bridge or the **HTTP** bridge.
- The MQTT/HTTP bridge is a central component of Cloud IoT Core, as shown in the components overview.

- MQTT is a standard publish/subscribe protocol that is frequently used and supported by embedded devices, and is also common in machine-to-machine interactions.

![](https://base.imgix.net/files/base/ebm/electronicdesign/image/2019/04/electronicdesign_13912_0915silabspromo.png?auto=format&fit=max&w=1200)

- HTTP is a "connectionless" protocol: with the HTTP bridge, devices do not maintain a connection to Cloud IoT Core. Instead, they send requests and receive responses. Cloud IoT Core supports HTTP 1.1 only (not 2.0)

![](https://images.slideplayer.com/24/7076230/slides/slide_16.jpg)

Below is the difference between MQTT AND HTTP

![](https://res.cloudinary.com/practicaldev/image/fetch/s--AA0c-xP8--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/n3ii2ka43wpar1h5ph2b.jpeg)

**REQUEST**

- Request in HTTP contains 3 parts - request line, HTTP Headers, Message Body.
- **GET request** - is a type of HTTP request using the GET method
- There are many different methods in the request. They are - 

- **GET** : Retrieve the resource from the server (e.g. when visiting a page)
- **POST** : Create a resource on the server (e.g. when submitting a form)
- **PUT/PATCH** : Update the resource on the server (used by APIs)
- **DELETE** : Delete the resource from the server (used by APIs)

![](https://image.slidesharecdn.com/mqttiotprotocolscomparison-140219090749-phpapp01/95/mqtt-iot-protocols-comparison-12-638.jpg?cb=1392817944)

**RESPONSE**

- It is comprised of 3 parts - Status line, HTTP Header, Message Body.
- Depending on the situation, the server sends different code.

   ![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTWx-0oB_ohpX3OgWpRZaUzzX_DrH7-iUb9QQ&usqp=CAU)









